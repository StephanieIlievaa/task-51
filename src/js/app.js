import "../scss/app.scss";

window.addEventListener("DOMContentLoaded", () => {
  // This block will be executed once the page is loaded and ready


  let nodeList = document.querySelectorAll('div.card, div.card active');

  let filtered = [...nodeList].filter(c => !c.className.includes('active'))

  setTimeout(function () {
    filtered.forEach(c => c.style.display = "none")
  }, 3000);

});
